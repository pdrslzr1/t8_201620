package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Clase que representa un grafo con peso no dirigido.
 * @author SamuelSalazar
 * @param <T> El tipo que se va a utilizar como nodos del grafo
 */
public class GrafoNoDirigido<T extends Nodo> implements Grafo<T> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos

    private List<Nodo> nodos;

	/**
	 * Lista de adyacencia 
	 */
	private HashMap<Integer, List<Arco>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado.

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
        adj = new HashMap<>();
        nodos = new ArrayList();
	}

	@Override
	public boolean agregarNodo(T nodo) {
		//TODO implementar
        if(adj.containsKey(nodo.darId()))
            return false;
        nodos.add(nodo);
        adj.put(nodo.darId(),new ArrayList<Arco>());
        return true;
    }

	@Override
	public boolean eliminarNodo(int id) {
		//TODO implementar
        boolean r = false;
        for (Nodo n:nodos) {
            if (n.darId() == id){
                nodos.remove(n);
                r = true;
                adj.remove(id);
                break;
            }
        }
        return r;
    }

	@Override
	public Arco[] darArcos() {
		//TODO implementar
        ArrayList<Arco> arc = new ArrayList<>();
        for (Nodo n :nodos) {
            for (Arco a:adj.get(n.darId())) {
                if(!arc.contains(a)){
                    arc.add(a);
                }
            }
        }
        return (Arco[]) arc.toArray();
    }

	private <E extends Comparable<E>> Arco crearArco( final int inicio, final int fin, final double costo, E e )
	{
		return new Arco(buscarNodo(inicio), buscarNodo(fin), costo, e);
	}

	@Override
	public Nodo[] darNodos() {
		//TODO implementar
        return (Nodo[]) nodos.toArray();
	}

	@Override
	public <E extends Comparable<E>> boolean agregarArco(int i, int f, double costo, E obj) {
		//TODO implementar

        Arco a = crearArco(i,f,costo,obj);
        Arco b = crearArco(f,i,costo,obj);
        if(a==null&&b==null) return false;
        adj.get(i).add(a);
        adj.get(f).add(b);
        return true;
	}

	@Override
	public boolean agregarArco(int i, int f, double costo) {

		return agregarArco(i, f, costo, null);
	}

	@Override
	public Arco eliminarArco(int inicio, int fin) {
		//TODO implementar
        Arco arc=null;
        for (Arco a: adj.get(inicio)) {
            if(a.darNodoFin().darId()==fin){
                arc = a;
                adj.get(inicio).remove(a);
                break;
            }
        }
        for (Arco a : adj.get(fin)) {
            if(a.darNodoInicio().darId() == inicio){
                adj.get(fin).remove(a);
            }
        }
        return arc;
    }

	@Override
	public Nodo buscarNodo(int id) {
		//TODO implementar
        for (Nodo n: nodos) {
            if(n.darId() == (id)){
                return n;
            }
        }
        return null;
    }

	@Override
	public Arco[] darArcosOrigen(int id) {
		//TODO implementar
        return (Arco[]) adj.get(id).toArray();
	}

	@Override
	public Arco[] darArcosDestino(int id) {
        ArrayList<Arco> arcos = new ArrayList();
		//TODO implementar
        for (Nodo n: nodos) {
            if(n.darId() != (id)){
                for (Arco a: adj.get(id)) {
                    if(a.darNodoFin().darId() == id)
                    {
                        arcos.add(a);
                        break;
                    }
                }
            }
        }
        return (Arco[]) arcos.toArray();
	}

}
