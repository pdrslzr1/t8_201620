package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema integrado de transporte
 * @author SamuelSalazar
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	/**
	 * Grafo que modela el sistema integrado de transporte
	 */
	//TODO Declare el grafo que va a modela el sistema
	private GrafoNoDirigido grafo;

	
	/**
	 * Construye un nuevo administrador del SITP
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
        grafo = new GrafoNoDirigido();
	}

	/**
	 * Devuelve todas las rutas que pasan por un paadero con nombre dado
	 * @param nombre 
	 * @return Arreglo con los nombres de las rutas que pasan por el estacion requerido
	 */
	public String[] darRutasParadero(String nombre)
	{
		//TODO Implementar
	}

	/**
	 * Devuelve la distancia que hay entre los paraderos mas cercanos del sistema.
	 * @return distancia minima entre 2 paraderos
	 */
	public double distanciaMinimaParaderos()
	{
		//TODO Implementar
	}
	
	
	public Estacion darParadero(int id)
	{
		//TODO Implementar
	}
	/**
	 * Devuelve la distancia que hay entre los paraderos más lejanos del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaParaderos()
	{
		//TODO Implementar
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
        BufferedReader br = new BufferedReader(new FileReader(RUTA_PARADEROS));
        int numero = Integer.parseInt(br.readLine());
        int contador = 0;
        while (contador <= numero){
            String[] s = br.readLine().split(";");
            Estacion e = new Estacion(contador,Double.parseDouble(s[2]),Double.parseDouble(s[1]),s[0]);
            grafo.agregarNodo(e);
            contador++;
        }
        br.close();
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");
		
		//TODO Implementar
        BufferedReader bfr = new BufferedReader(new FileReader(RUTA_RUTAS));
        int numeroR = Integer.parseInt(bfr.readLine());

		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" Rutas");
	}

	/**
	 * Busca el estacion con nombre dado<br>
	 * @param nombre el nombre del estacion buscado
	 * @return estacion cuyo nombre coincide con el parametro, null de lo contrario
	 */
	public Estacion buscarParadero(String nombre)
	{
		//TODO Implementar
	}

}
